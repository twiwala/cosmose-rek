package pl.dram.cosmosepr.booking.repository;

import pl.dram.cosmosepr.booking.dto.AvailableRoomDto;
import pl.dram.cosmosepr.booking.dto.RoomSearchCriteriaDto;

import java.util.List;

public interface HotelRoomRepositoryCustom {
    List<AvailableRoomDto> findAvailableRooms(RoomSearchCriteriaDto roomSearchCriteriaDto);
}
