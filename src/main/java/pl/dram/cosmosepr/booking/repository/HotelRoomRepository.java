package pl.dram.cosmosepr.booking.repository;

import org.springframework.data.repository.CrudRepository;
import pl.dram.cosmosepr.booking.model.HotelRoom;

public interface HotelRoomRepository extends CrudRepository<HotelRoom, Long>, HotelRoomRepositoryCustom {

}
