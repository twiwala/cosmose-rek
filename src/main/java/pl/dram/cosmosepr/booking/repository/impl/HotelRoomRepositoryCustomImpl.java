package pl.dram.cosmosepr.booking.repository.impl;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.SubQueryExpression;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;
import pl.dram.cosmosepr.booking.dto.AvailableRoomDto;
import pl.dram.cosmosepr.booking.dto.RoomSearchCriteriaDto;
import pl.dram.cosmosepr.booking.model.*;
import pl.dram.cosmosepr.booking.repository.HotelRoomRepositoryCustom;
import pl.dram.cosmosepr.common.tool.ConditionsBuilder;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

@AllArgsConstructor
@Repository
public class HotelRoomRepositoryCustomImpl implements HotelRoomRepositoryCustom {
    private EntityManager entityManager;

    private static final QHotel hotel = QHotel.hotel;
    private static final QHotelRoom hotelRoom = QHotelRoom.hotelRoom;
    private static final QReservation reservation = QReservation.reservation;
    @Override
    public List<AvailableRoomDto> findAvailableRooms(RoomSearchCriteriaDto roomSearchCriteriaDto) {
        JPQLQuery<HotelRoom> query = prepareFindAvailableRoomsQuery(roomSearchCriteriaDto);
        return query.select(Projections.bean(AvailableRoomDto.class,
                Collections.singletonMap("roomId", hotelRoom.id)
        )).distinct().fetch();//add offset & limit, for pagination
    }

    private JPQLQuery<HotelRoom> prepareFindAvailableRoomsQuery(RoomSearchCriteriaDto criteria){
        JPQLQuery<HotelRoom> hotelRoomQuery = new JPAQuery<>(entityManager);

        BooleanBuilder reservationsInDatesFilter = new ConditionsBuilder()
                .orIfBuilder(criteria.getFromDate() != null, () -> new ConditionsBuilder()
                        .and(reservation.fromDate.loe(criteria.getFromDate()))
                        .and(reservation.toDate.goe(criteria.getFromDate()))
                        .generate()
                )
                .orIfBuilder(criteria.getToDate() != null, () -> new ConditionsBuilder()
                        .and(reservation.fromDate.loe(criteria.getToDate()))
                        .and(reservation.toDate.goe(criteria.getToDate()))
                        .generate()
                )
                .generate();

        SubQueryExpression<HotelRoom> reservationQuery = new JPAQuery<>(entityManager)
                .select(reservation.hotelRoom).from(reservation).where(reservationsInDatesFilter);

        BooleanBuilder searchFilter = new ConditionsBuilder()
                .andIf(!criteria.getCity().isEmpty(), () -> hotelRoom.hotel.city.eq(criteria.getCity()))
                .andIf(criteria.getPriceMin() != null, () -> hotelRoom.dailyPrice.gt(criteria.getPriceMin()))
                .andIf(criteria.getPriceMax() != null, () -> hotelRoom.dailyPrice.lt(criteria.getPriceMax()))
                .andIf(criteria.getFromDate() != null || criteria.getToDate() != null,
                        () -> hotelRoom.notIn(reservationQuery))
                .generate();


        BooleanBuilder where = new ConditionsBuilder().and(searchFilter).and(hotelRoom.isAvailable.eq(true)).generate();

        return hotelRoomQuery.from(hotelRoom)
                .innerJoin(hotelRoom.hotel, hotel).on(hotelRoom.hotel.id.eq(hotel.id))
                .leftJoin(hotelRoom.reservations, reservation).on(hotelRoom.id.eq(reservation.hotelRoom.id))
                .where(where);
    }
}
