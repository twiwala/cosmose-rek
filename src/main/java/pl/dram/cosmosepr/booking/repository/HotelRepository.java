package pl.dram.cosmosepr.booking.repository;

import org.springframework.data.repository.CrudRepository;
import pl.dram.cosmosepr.booking.model.Hotel;

public interface HotelRepository extends CrudRepository<Hotel, Long> {
}
