package pl.dram.cosmosepr.booking.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import pl.dram.cosmosepr.booking.model.HotelRoom;
import pl.dram.cosmosepr.booking.model.Reservation;
import pl.dram.cosmosepr.core.model.User;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Stream;

public interface ReservationReposiotory extends CrudRepository<Reservation, Long> {
    @Query("SELECT r FROM Reservation r WHERE r.hotelRoom = ?1 AND ((r.fromDate < ?2 AND r.toDate > ?2) OR (r.fromDate < ?3 AND r.toDate > ?3))")
    Stream<Reservation> findReservationsConflictingWithDates(HotelRoom hotelRoom, LocalDate from, LocalDate to);

    Stream<Reservation> findByCustomer(@NotNull User customer);
}
