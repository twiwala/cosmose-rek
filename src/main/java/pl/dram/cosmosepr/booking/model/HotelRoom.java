package pl.dram.cosmosepr.booking.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "HOTEL_ROOM")
public class HotelRoom {

    @SequenceGenerator(name = "HOTEL_ROOM_SEQ_GEN", sequenceName = "HOTEL_ROOM_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "HOTEL_ROOM_SEQ_GEN")
    @Id
    private Long id;

    @DecimalMin(value = "0.0")
    @Column(nullable = false, precision = 7, scale = 2)
    @Digits(integer = 9, fraction = 2)
    private BigDecimal dailyPrice;

    @Column(nullable = false)
    private Boolean isAvailable = true;

    @ManyToOne
    @JoinColumn(name = "hotel_id", nullable = false)
    private Hotel hotel;

    @OneToMany(mappedBy = "hotelRoom", orphanRemoval = true, cascade = CascadeType.ALL)
    List<Reservation> reservations = new ArrayList<>();

    public void addReservation(Reservation reservation){
        reservation.setHotelRoom(this);
        reservation.setDailyPrice(dailyPrice);
        reservations.add(reservation);
    }
}
