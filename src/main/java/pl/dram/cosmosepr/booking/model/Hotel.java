package pl.dram.cosmosepr.booking.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.dram.cosmosepr.core.exception.CosmoseException;
import pl.dram.cosmosepr.core.model.User;
import pl.dram.cosmosepr.core.model.UserType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static pl.dram.cosmosepr.core.exception.CosmoseError.USER_HAS_INVALID_PRIVILLAGES;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "HOTEL")
public class Hotel {
    @SequenceGenerator(name = "HOTEL_SEQ_GEN", sequenceName = "HOTEL_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "HOTEL_SEQ_GEN")
    @Id
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String city;


    @ManyToMany
    @JoinTable(
            name = "hotel_owners",
            joinColumns = @JoinColumn(name = "hotel_id"),
            inverseJoinColumns = @JoinColumn(name = "owner_id")
    )
    private final List<User> owners = new ArrayList<>();


    @OneToMany(mappedBy = "hotel", orphanRemoval = true, cascade = CascadeType.ALL)
    private final List<HotelRoom> hotelRooms = new ArrayList<>();

    public void addOwner(User user){
        if(user.getUserType() != UserType.OWNER){
            throw new CosmoseException(USER_HAS_INVALID_PRIVILLAGES);
        }
        owners.add(user);
    }
    public void addHotelRoom(HotelRoom hotelRoom){
        hotelRoom.setHotel(this);
        hotelRooms.add(hotelRoom);
    }
}
