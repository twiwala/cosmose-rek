package pl.dram.cosmosepr.booking.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.dram.cosmosepr.core.model.User;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "RESERVATION")
public class Reservation {
    @SequenceGenerator(name = "RESERVATION_SEQ_GEN", sequenceName = "RESERVATION_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RESERVATION_SEQ_GEN")
    @Id
    private Long id;

    @Column(nullable = false)
    private LocalDate fromDate;

    @Column(nullable = false)
    private LocalDate toDate;

    @DecimalMin(value = "0.0")
    @Column(nullable = false, precision = 7, scale = 2)
    @Digits(integer = 9, fraction = 2)
    private BigDecimal dailyPrice; //in case if room price change after reservation

    @ManyToOne
    @JoinColumn(nullable = false)
    private HotelRoom hotelRoom;

    @OneToOne
    @JoinColumn(nullable = false)
    private User customer;
}
