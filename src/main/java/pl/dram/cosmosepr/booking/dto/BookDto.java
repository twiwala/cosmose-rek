package pl.dram.cosmosepr.booking.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class BookDto {
    private Long roomId;
    private LocalDate fromDate;
    private LocalDate toDate;
}
