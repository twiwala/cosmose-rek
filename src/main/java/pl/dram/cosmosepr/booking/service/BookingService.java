package pl.dram.cosmosepr.booking.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.dram.cosmosepr.booking.dto.AvailableRoomDto;
import pl.dram.cosmosepr.booking.dto.BookDto;
import pl.dram.cosmosepr.booking.dto.RoomSearchCriteriaDto;
import pl.dram.cosmosepr.booking.model.HotelRoom;
import pl.dram.cosmosepr.booking.model.Reservation;
import pl.dram.cosmosepr.booking.repository.HotelRoomRepository;
import pl.dram.cosmosepr.booking.repository.ReservationReposiotory;
import pl.dram.cosmosepr.booking.validation.BookingValidator;
import pl.dram.cosmosepr.core.model.User;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class BookingService {
    private BookingValidator bookingValidator;
    private HotelRoomRepository hotelRoomRepository;
    private ReservationReposiotory reservationReposiotory;
    private EntityManager entityManager;

    @Transactional
    public Long createBooking(BookDto bookDto, Long userId) {
        bookingValidator.validateNewReservation(bookDto, userId);
        Optional<HotelRoom> hotelRoom = hotelRoomRepository.findById(bookDto.getRoomId());

        Reservation reservation = Reservation.builder()
                .customer(User.builder().id(userId).build())
                .fromDate(bookDto.getFromDate())
                .hotelRoom(hotelRoom.get())
                .dailyPrice(hotelRoom.get().getDailyPrice())
                .toDate(bookDto.getToDate())
                .build();

        reservationReposiotory.save(reservation);
        return reservation.getId();
    }

    @Transactional(readOnly = true)
    public List<AvailableRoomDto> searchBooking(RoomSearchCriteriaDto roomSearchCriteriaDto) {
        List<AvailableRoomDto> availableRooms = hotelRoomRepository.findAvailableRooms(roomSearchCriteriaDto);
        return availableRooms;
    }

    @Transactional
    public Boolean deleteBooking(Long bookingId) {
        reservationReposiotory.deleteById(bookingId);
        return true;
    }
    @Transactional(readOnly = true)
    public List<BookDto> getBookingForUser(Long userId) {
        Stream<Reservation> byCustomer = reservationReposiotory.findByCustomer(User.builder().id(userId).build());
        List<BookDto> collect = byCustomer.map(r -> new BookDto(r.getHotelRoom().getId(), r.getFromDate(), r.getToDate()))
                .collect(Collectors.toList());
        return collect;
    }
}
