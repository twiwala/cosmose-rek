package pl.dram.cosmosepr.booking.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.dram.cosmosepr.booking.dto.AvailableRoomDto;
import pl.dram.cosmosepr.booking.dto.BookDto;
import pl.dram.cosmosepr.booking.dto.RoomSearchCriteriaDto;
import pl.dram.cosmosepr.booking.service.BookingService;

import java.util.List;

@RestController
@RequestMapping("booking")
@AllArgsConstructor
public class BookingController {
    private BookingService bookingService;

    //user should be provided from security context as user object instead of parameter
    @GetMapping("/room/{userId}") // considering above -> @GetMapping("/room/me")
    public ResponseEntity<List<BookDto>> getUserBooking(@PathVariable Long userId){
        return ResponseEntity.ok(bookingService.getBookingForUser(userId));
    }
    @DeleteMapping("/room/{bookingId}") //there should be parameter from security context, and check
    //if user can remove reservation i.e. 1) USer = hotel owner or 2) user is reservation owner
    public ResponseEntity<Boolean> deleteBooking(@PathVariable Long bookingId){
        return ResponseEntity.ok(bookingService.deleteBooking(bookingId));
    }

    @GetMapping("/room/search")//could be pageable
    public ResponseEntity<List<AvailableRoomDto>> searchBooking(RoomSearchCriteriaDto roomSearchCriteriaDto){
        return ResponseEntity.ok(bookingService.searchBooking(roomSearchCriteriaDto));
    }

    @PostMapping("/room/{userId}")
    public ResponseEntity<Long> createBooking(@RequestBody BookDto bookDto, @PathVariable Long userId){
        return ResponseEntity.ok(bookingService.createBooking(bookDto, userId));
    }


}
