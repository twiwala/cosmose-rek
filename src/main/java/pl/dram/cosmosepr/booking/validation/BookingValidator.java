package pl.dram.cosmosepr.booking.validation;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import pl.dram.cosmosepr.booking.dto.BookDto;
import pl.dram.cosmosepr.booking.model.HotelRoom;
import pl.dram.cosmosepr.booking.model.Reservation;
import pl.dram.cosmosepr.booking.repository.HotelRoomRepository;
import pl.dram.cosmosepr.booking.repository.ReservationReposiotory;
import pl.dram.cosmosepr.core.exception.CosmoseException;
import pl.dram.cosmosepr.core.model.User;
import pl.dram.cosmosepr.core.repository.UserRepository;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.Optional;
import java.util.stream.Stream;

import static pl.dram.cosmosepr.core.exception.CosmoseError.*;

@Component
@AllArgsConstructor
public class BookingValidator {
    UserRepository userRepository;

    HotelRoomRepository hotelRoomRepository;

    ReservationReposiotory reservationReposiotory;

    public boolean validateNewReservation(BookDto bookDto, Long userId){
        Optional<HotelRoom> hotelRoomOptional = hotelRoomRepository.findById(bookDto.getRoomId());
        HotelRoom hotelRoom = hotelRoomOptional.orElse(null);

        if(!isDateFromSmallerThanDateTo(bookDto)){
            throw new CosmoseException(BOOKING_PROVIDED_DATES_NOT_VALID);
        }
        if(isDateInThePast(bookDto)){
           throw new CosmoseException(BOOKING_NOT_POSSIBLE_IN_THE_PAST);
        }
        if(!isRoomExist(hotelRoom)){
            throw new CosmoseException(BOOKING_ROOM_NOT_FOUND);
        }
        if(!isRoomActive(hotelRoom)){
            throw new CosmoseException(BOOKING_ROOM_IS_NOT_ACTIVE);
        }
        if(!isUserExists(userId)){
            throw new CosmoseException(USER_NOT_EXISTS);
        }
        if(isRoomReservedInSelectedTime(hotelRoom, bookDto)){
            throw new CosmoseException(BOOKING_ROOM_IS_RESERVED_BY_ANOTHER_USER);
        }

        return true;
    }

    private boolean isDateInThePast(BookDto bookDto) {
        return bookDto.getFromDate().compareTo(LocalDate.now()) < 0;
    }

    private boolean isRoomExist(HotelRoom hotelRoom){
        return hotelRoom != null;
    }
    private boolean isDateFromSmallerThanDateTo(BookDto bookDto){
        int i = bookDto.getFromDate().compareTo(bookDto.getToDate());
        return i < 0;
    }
    private boolean isRoomActive(HotelRoom hotelRoom){
        return hotelRoom.getIsAvailable();
    }
    private boolean isRoomReservedInSelectedTime(HotelRoom hotelRoom, BookDto bookDto) {
        Stream<Reservation> reservationsConflictingWithDates =  reservationReposiotory.findReservationsConflictingWithDates(hotelRoom, bookDto.getFromDate(), bookDto.getToDate());
        return reservationsConflictingWithDates.findFirst().isPresent();
    }
    private boolean isUserExists(Long userId){
        Optional<User> byId = userRepository.findById(userId);
        return byId.isPresent();
    }
}
