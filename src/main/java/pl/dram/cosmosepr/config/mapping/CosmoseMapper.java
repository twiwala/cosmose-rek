package pl.dram.cosmosepr.config.mapping;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class CosmoseMapper {

    @Bean
    public MapperFacade getMapper(){
        return new DefaultMapperFactory.Builder().build().getMapperFacade();
    }
}
