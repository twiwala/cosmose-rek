package pl.dram.cosmosepr.common.tool;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;


public class ConditionsBuilder {
    private BooleanBuilder builder;

    public ConditionsBuilder() {
        this.builder = new BooleanBuilder();
    }

    public ConditionsBuilder and(Predicate right){
        builder.and(right);
        return this;
    }
    public ConditionsBuilder or(Predicate right){
        builder.or(right);
        return this;
    }
    public ConditionsBuilder andIf(boolean value, LazyBooleanExpression expression){
        if(value){
            builder.and(expression.get());
        }
        return this;
    }
    public ConditionsBuilder orIf(boolean value, LazyBooleanExpression expression){
        if(value) {
            builder.or(expression.get());
        }
        return this;
    }
    public ConditionsBuilder orIfBuilder(boolean value, LazyBooleanBuilder b){
        if(value) {
            builder.or(b.get().getValue());
        }
        return this;
    }
    public BooleanBuilder generate(){
        return builder;
    }

    @FunctionalInterface
    public interface LazyBooleanExpression {
        BooleanExpression get();
    }
    @FunctionalInterface
    public interface LazyBooleanBuilder {
        BooleanBuilder get();
    }
}
