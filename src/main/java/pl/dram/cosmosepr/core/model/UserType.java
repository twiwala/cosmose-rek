package pl.dram.cosmosepr.core.model;

public enum UserType {
    OWNER, CUSTOMER;
}
