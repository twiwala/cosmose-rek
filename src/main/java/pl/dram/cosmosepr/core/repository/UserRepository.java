package pl.dram.cosmosepr.core.repository;

import org.springframework.data.repository.CrudRepository;
import pl.dram.cosmosepr.core.model.User;

public interface UserRepository extends CrudRepository<User, Long> {
}
