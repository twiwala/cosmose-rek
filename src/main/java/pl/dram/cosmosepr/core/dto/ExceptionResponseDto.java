package pl.dram.cosmosepr.core.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class ExceptionResponseDto {
    private String errorCode;
    private String errorMessage;
}
