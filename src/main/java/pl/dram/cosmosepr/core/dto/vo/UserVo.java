package pl.dram.cosmosepr.core.dto.vo;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
public class UserVo {

    @Length(min = 1, max = 255)
    @NotNull
    private String email;

    @Length(min = 1, max = 255)
    @NotNull
    private String password;
}
