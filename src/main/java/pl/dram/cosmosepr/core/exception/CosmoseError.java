package pl.dram.cosmosepr.core.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
public enum CosmoseError {

    BOOKING_ROOM_NOT_FOUND("Room with selected criteria cannot be reserved", HttpStatus.BAD_REQUEST),
    BOOKING_ROOM_IS_NOT_ACTIVE("Cannot reserve selected room, because is not active", HttpStatus.BAD_REQUEST),
    BOOKING_PROVIDED_DATES_NOT_VALID("Date from is greater than date to", HttpStatus.BAD_REQUEST),
    BOOKING_ROOM_IS_RESERVED_BY_ANOTHER_USER("Booking is reserved by another user", HttpStatus.BAD_REQUEST),
    BOOKING_NOT_POSSIBLE_IN_THE_PAST("Booking is not possible in the past", HttpStatus.BAD_REQUEST),

    USER_HAS_INVALID_PRIVILLAGES("User has invalid privillages", HttpStatus.BAD_REQUEST),
    USER_NOT_EXISTS("User not exists", HttpStatus.BAD_REQUEST);

    private String message;
    private HttpStatus status;
}
