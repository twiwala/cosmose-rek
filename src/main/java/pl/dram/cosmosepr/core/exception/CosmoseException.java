package pl.dram.cosmosepr.core.exception;

import lombok.Getter;

@Getter
public class CosmoseException extends RuntimeException {
    private CosmoseError error;
    private String message;

    public CosmoseException(CosmoseError error){
        super(error.getMessage());
        this.error = error;
        this.message = error.getMessage();
    }
}
