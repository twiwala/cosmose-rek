package pl.dram.cosmosepr.core.service;

import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.stereotype.Service;
import pl.dram.cosmosepr.core.dto.UserDto;
import pl.dram.cosmosepr.core.model.User;
import pl.dram.cosmosepr.core.repository.UserRepository;

import javax.transaction.Transactional;

@Service
@AllArgsConstructor
public class UserService {
    private UserRepository userRepository;

    private MapperFacade mapper;

    @Transactional
    public UserDto register(User user) {
        userRepository.save(user);

        UserDto userDto = mapper.map(user, UserDto.class);
        return userDto;
    }
}
