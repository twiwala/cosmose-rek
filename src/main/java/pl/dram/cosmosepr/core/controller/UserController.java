package pl.dram.cosmosepr.core.controller;

import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.dram.cosmosepr.core.dto.UserDto;
import pl.dram.cosmosepr.core.dto.vo.UserVo;
import pl.dram.cosmosepr.core.model.User;
import pl.dram.cosmosepr.core.service.UserService;

import javax.validation.Valid;

@RestController
@RequestMapping("users")
@AllArgsConstructor
public class UserController {
    private UserService userService;

    private MapperFacade mapper;

    @PostMapping("/register")
    public ResponseEntity<UserDto> register(@Valid @RequestBody UserVo userVo){
        return ResponseEntity.ok(userService.register(mapper.map(userVo, User.class)));
    }
}
