package pl.dram.cosmosepr.core.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import pl.dram.cosmosepr.core.dto.ExceptionResponseDto;
import pl.dram.cosmosepr.core.exception.CosmoseException;

@ControllerAdvice
public class ExceptionHandlingControllerAdvice {

    @ExceptionHandler(CosmoseException.class)
    public ResponseEntity<ExceptionResponseDto> exceptionThrown(CosmoseException ex){
        return ResponseEntity.status(ex.getError().getStatus()).body(
                ExceptionResponseDto.builder()
                        .errorCode(ex.getError().name())
                        .errorMessage(ex.getMessage())
                        .build()
        );
    }
}
