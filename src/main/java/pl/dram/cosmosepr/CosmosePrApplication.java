package pl.dram.cosmosepr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CosmosePrApplication {

	public static void main(String[] args) {
		SpringApplication.run(CosmosePrApplication.class, args);
	}

}
