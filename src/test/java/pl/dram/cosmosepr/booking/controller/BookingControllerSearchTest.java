package pl.dram.cosmosepr.booking.controller;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import pl.dram.cosmosepr.BaseIntegrationTest;
import pl.dram.cosmosepr.booking.model.Hotel;
import pl.dram.cosmosepr.booking.model.HotelRoom;
import pl.dram.cosmosepr.booking.repository.HotelRepository;
import pl.dram.cosmosepr.booking.repository.HotelRoomRepository;
import pl.dram.cosmosepr.core.model.User;
import pl.dram.cosmosepr.core.model.UserType;
import pl.dram.cosmosepr.core.repository.UserRepository;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.Matchers.*;

public class BookingControllerSearchTest extends BaseIntegrationTest {
    private static LocalDate fromDateInFuture = LocalDate.now().plusDays(20);
    private static LocalDate toDateInFuture = LocalDate.now().plusDays(40);

    private static String API_BOOKING_ROOM_SEARCH_PATH = "/booking/room/search";
    private static String API_BOOKING_ROOM = "/booking/room/";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private HotelRepository hotelRepository;
    @Autowired
    private HotelRoomRepository hotelRoomRepository;

    private User owner;
    private Hotel hotel;
    private HotelRoom hotelRoom;
    private HotelRoom hotelRoom2;

    @BeforeEach
    void beforeEach() {
        owner = User.builder()
                .userType(UserType.OWNER)
                .email("test@test.pl")
                .password("xyz")
                .build();

        userRepository.save(owner);

        hotel = Hotel.builder()
                .name("Test")
                .city("Warszawa")
                .build();

        hotelRoom = HotelRoom.builder()
                .isAvailable(true)
                .dailyPrice(BigDecimal.ONE)
                .build();

        hotelRoom2 = HotelRoom.builder()
                .isAvailable(true)
                .dailyPrice(new BigDecimal(50))
                .build();

        hotel.addOwner(owner);
        hotel.addHotelRoom(hotelRoom);
        hotel.addHotelRoom(hotelRoom2);

        hotelRepository.save(hotel);
    }

    @AfterEach
    void afterEach() {
        hotelRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    @DisplayName("Should return available rooms when no reservations")
    void shouldGetRoomsWhenNoReservation() throws Exception {
        mockMvc.perform(get(API_BOOKING_ROOM_SEARCH_PATH))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].roomId", is(hotelRoom.getId().intValue())))
                .andExpect(jsonPath("$[1].roomId", is(hotelRoom2.getId().intValue())));

    }

    @Test
    @DisplayName("Should return no rooms when city not found")
    void shouldReturnNoRoomsIfCityNotFound() throws Exception {
        mockMvc.perform(get(API_BOOKING_ROOM_SEARCH_PATH)
                .param("city", "XYZ"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    @DisplayName("Should return rooms when city found")
    void shouldFilterRoomsByCity() throws Exception {
        mockMvc.perform(get(API_BOOKING_ROOM_SEARCH_PATH)
                .param("city", "Warszawa"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].roomId", is(hotelRoom.getId().intValue())))
                .andExpect(jsonPath("$[1].roomId", is(hotelRoom2.getId().intValue())));
    }


    @Test
    @DisplayName("Should return room when filter by daily price and city")
    void shouldFilterRoomsByDailyPrice() throws Exception {
        mockMvc.perform(get(API_BOOKING_ROOM_SEARCH_PATH)
                .param("city", "Warszawa")
                .param("priceMin", "5.0")
                .param("priceMax", "70.0"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].roomId", is(hotelRoom2.getId().intValue())));
    }

    @Test
    @DisplayName("Should return room when filter by reservation period")
    void shouldFilterRoomsByReservationPeriod() throws Exception {
        //reserve first room
        String newReservationJson1 = "{ \"roomId\": " + hotelRoom.getId() + ", \"fromDate\": \""+fromDateInFuture+"\", \"toDate\": \""+toDateInFuture+"\"}";

        mockMvc.perform(post(API_BOOKING_ROOM + owner.getId())
                .content(newReservationJson1)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mockMvc.perform(get(API_BOOKING_ROOM_SEARCH_PATH)
                .param("fromDate", fromDateInFuture.plusDays(1).toString())
                .param("toDate", toDateInFuture.toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].roomId", is(hotelRoom2.getId().intValue())));
    }

    @Test
    @DisplayName("Should return room when filter by reservation period and city")
    void shouldReturnRoomWhenFilterByReservationPeriodAndCity() throws Exception {
        //reserve first room
        String newReservationJson1 = "{ \"roomId\": " + hotelRoom.getId() + ", \"fromDate\": \""+fromDateInFuture+"\", \"toDate\": \""+toDateInFuture+"\"}";

        mockMvc.perform(post(API_BOOKING_ROOM + owner.getId())
                .content(newReservationJson1)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mockMvc.perform(get(API_BOOKING_ROOM_SEARCH_PATH)
                .param("city", "Warszawa")
                .param("fromDate", fromDateInFuture.plusDays(1).toString())
                .param("toDate", toDateInFuture.toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].roomId", is(hotelRoom2.getId().intValue())));
    }

    @Test
    @DisplayName("Should return room when filter by period in between reservations")
    void shouldFilterRoomsByPeriodInBetweenDates() throws Exception {
        //reserve first room
        String newReservationJson1 = "{ \"roomId\": " + hotelRoom.getId() + ", \"fromDate\": \""+fromDateInFuture+"\", \"toDate\": \""+toDateInFuture+"\"}";
        String newReservationJson2 = "{ \"roomId\": " + hotelRoom.getId() + ", \"fromDate\": \""+toDateInFuture.plusDays(20)+"\", \"toDate\": \""+toDateInFuture.plusDays(30)+"\"}";

        mockMvc.perform(post(API_BOOKING_ROOM + owner.getId())
                .content(newReservationJson1)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mockMvc.perform(post(API_BOOKING_ROOM + owner.getId())
                .content(newReservationJson2)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mockMvc.perform(get(API_BOOKING_ROOM_SEARCH_PATH)
                .param("fromDate", toDateInFuture.plusDays(10).toString())
                .param("toDate", toDateInFuture.plusDays(15).toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].roomId", is(hotelRoom.getId().intValue())))
                .andExpect(jsonPath("$[1].roomId", is(hotelRoom2.getId().intValue())));
    }
}
