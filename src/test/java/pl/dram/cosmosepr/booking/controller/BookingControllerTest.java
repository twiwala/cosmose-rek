package pl.dram.cosmosepr.booking.controller;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import pl.dram.cosmosepr.BaseIntegrationTest;
import pl.dram.cosmosepr.booking.model.Hotel;
import pl.dram.cosmosepr.booking.model.HotelRoom;
import pl.dram.cosmosepr.booking.repository.HotelRepository;
import pl.dram.cosmosepr.booking.repository.HotelRoomRepository;
import pl.dram.cosmosepr.core.model.User;
import pl.dram.cosmosepr.core.model.UserType;
import pl.dram.cosmosepr.core.repository.UserRepository;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class BookingControllerTest extends BaseIntegrationTest {
    private static LocalDate fromDateInFuture = LocalDate.now().plusDays(20);
    private static LocalDate toDateInFuture = LocalDate.now().plusDays(40);

    private static String API_BOOKING_ROOM = "/booking/room/";
    
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private HotelRepository hotelRepository;
    @Autowired
    private HotelRoomRepository hotelRoomRepository;

    private User owner;
    private Hotel hotel;
    private HotelRoom hotelRoom;

    @BeforeEach
    void beforeEach() {
        owner = User.builder()
                .userType(UserType.OWNER)
                .email("test@test.pl")
                .password("xyz")
                .build();

        userRepository.save(owner);

        hotel = Hotel.builder()
                .name("Test")
                .city("Warszawa")
                .build();

        hotelRoom = HotelRoom.builder()
                .isAvailable(true)
                .dailyPrice(BigDecimal.ONE)
                .build();

        hotel.addOwner(owner);
        hotel.addHotelRoom(hotelRoom);

        hotelRepository.save(hotel);

    }

    @AfterEach
    void afterEach() {
        hotelRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    @DisplayName("Should create reservation")
    void shouldCreateReservation() throws Exception {
        String newReservationJson = "{ \"roomId\": " + hotelRoom.getId() + ", \"fromDate\": \""+fromDateInFuture+"\", \"toDate\": \""+toDateInFuture+"\"}";

        mockMvc.perform(post(API_BOOKING_ROOM + owner.getId())
                .content(newReservationJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    @Test
    @DisplayName("Should create two reservations")
    void shouldCreateTwoReservations() throws Exception {
        String newReservationJson1 = "{ \"roomId\": " + hotelRoom.getId() + ", \"fromDate\": \""+fromDateInFuture+"\", \"toDate\": \""+toDateInFuture+"\"}";

        String newReservationJson2 = "{ \"roomId\": " + hotelRoom.getId() + ", \"fromDate\": \""+toDateInFuture.plusDays(5)+"\", \"toDate\": \""+toDateInFuture.plusDays(7)+"\"}";

        mockMvc.perform(post(API_BOOKING_ROOM + owner.getId())
                .content(newReservationJson1)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mockMvc.perform(post(API_BOOKING_ROOM + owner.getId())
                .content(newReservationJson2)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Should create reservation between two reservations")
    void shouldCreateBetweenTwoReservations() throws Exception {
        String newReservationJson1 = "{ \"roomId\": " + hotelRoom.getId() + ", \"fromDate\": \""+fromDateInFuture+"\", \"toDate\": \""+toDateInFuture+"\"}";

        String newReservationJson2 = "{ \"roomId\": " + hotelRoom.getId() + ", \"fromDate\": \""+toDateInFuture.plusDays(5)+"\", \"toDate\": \""+toDateInFuture.plusDays(7)+"\"}";

        String newReservationJson3 = "{ \"roomId\": " + hotelRoom.getId() + ", \"fromDate\": \""+toDateInFuture.plusDays(9)+"\", \"toDate\": \""+toDateInFuture.plusDays(15)+"\"}";

        mockMvc.perform(post(API_BOOKING_ROOM + owner.getId())
                .content(newReservationJson1)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mockMvc.perform(post(API_BOOKING_ROOM + owner.getId())
                .content(newReservationJson2)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mockMvc.perform(post(API_BOOKING_ROOM + owner.getId())
                .content(newReservationJson3)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Should not create reservation during collision")
    void shouldNotCreateReservationDuringCollision() throws Exception {
        String newReservationJson1 = "{ \"roomId\": " + hotelRoom.getId() + ", \"fromDate\": \""+fromDateInFuture+"\", \"toDate\": \""+toDateInFuture+"\"}";

        String newReservationJson2 = "{ \"roomId\": " + hotelRoom.getId() + ", \"fromDate\": \""+fromDateInFuture.plusDays(1)+"\", \"toDate\": \""+toDateInFuture.minusDays(1)+"\"}";

        mockMvc.perform(post(API_BOOKING_ROOM + owner.getId())
                .content(newReservationJson1)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mockMvc.perform(post(API_BOOKING_ROOM + owner.getId())
                .content(newReservationJson2)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

    }

    @Test
    @DisplayName("Should not create reservation during date from collision")
    void shouldNotCreateReservationDuringDateFromCollision() throws Exception {
        String newReservationJson1 = "{ \"roomId\": " + hotelRoom.getId() + ", \"fromDate\": \""+fromDateInFuture+"\", \"toDate\": \""+toDateInFuture+"\"}";

        String newReservationJson2 = "{ \"roomId\": " + hotelRoom.getId() + ", \"fromDate\": \""+fromDateInFuture.plusDays(1)+"\", \"toDate\": \""+toDateInFuture.plusDays(10)+"\"}";

        mockMvc.perform(post(API_BOOKING_ROOM + owner.getId())
                .content(newReservationJson1)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mockMvc.perform(post(API_BOOKING_ROOM + owner.getId())
                .content(newReservationJson2)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

    }

    @Test
    @DisplayName("Should not create reservation during date to collision")
    void shouldNotCreateReservationDuringDateToCollision() throws Exception {
        String newReservationJson1 = "{ \"roomId\": " + hotelRoom.getId() + ", \"fromDate\": \""+fromDateInFuture+"\", \"toDate\": \""+toDateInFuture+"\"}";

        String newReservationJson2 = "{ \"roomId\": " + hotelRoom.getId() + ", \"fromDate\": \"2019-03-01\", \"toDate\": \"2019-03-04\"}";

        mockMvc.perform(post(API_BOOKING_ROOM + owner.getId())
                .content(newReservationJson1)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mockMvc.perform(post(API_BOOKING_ROOM + owner.getId())
                .content(newReservationJson2)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    @DisplayName("Should not create reservation when user not found")
    void shouldNotCreateReservationWhenUserNotFound() throws Exception {
        String newReservationJson1 = "{ \"roomId\": " + hotelRoom.getId() + ", \"fromDate\": \""+fromDateInFuture+"\", \"toDate\": \""+toDateInFuture+"\"}";

        mockMvc.perform(post(API_BOOKING_ROOM + (owner.getId() + 100))
                .content(newReservationJson1)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

    }

    @Test
    @DisplayName("Should not create reservation when room not exist")
    void shouldNotCreateReservationWhenRoomNotExist() throws Exception {
        String newReservationJson1 = "{ \"roomId\": " + (hotelRoom.getId()+100) + ", \"fromDate\": \""+fromDateInFuture+"\", \"toDate\": \""+toDateInFuture+"\"}";

        mockMvc.perform(post(API_BOOKING_ROOM + owner.getId())
                .content(newReservationJson1)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    @DisplayName("Should not create reservation when date from is higher than date to")
    void shouldNotCreateReservationWhenDateFromIsHigherThanDateTo() throws Exception {
        String newReservationJson1 = "{ \"roomId\": " + hotelRoom.getId() + ", \"fromDate\": \""+toDateInFuture+"\", \"toDate\": \""+fromDateInFuture+"\"}";

        mockMvc.perform(post(API_BOOKING_ROOM + owner.getId())
                .content(newReservationJson1)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    @DisplayName("Should not create reservation when date from is in the past")
    void shouldNotCreateReservationWhenDateFromInThePast() throws Exception {
        String newReservationJson1 = "{ \"roomId\": " + hotelRoom.getId() + ", \"fromDate\": \""+LocalDate.now().minusDays(2)+"\", \"toDate\": \""+fromDateInFuture+"\"}";

        mockMvc.perform(post(API_BOOKING_ROOM + owner.getId())
                .content(newReservationJson1)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }
    @Test
    @DisplayName("Should not create reservation when room is not active")
    void shouldNotCreateReservationWhenRoomIsNotActive() throws Exception {
        HotelRoom hotelRoom2 = HotelRoom.builder()
                .isAvailable(false)
                .dailyPrice(BigDecimal.ONE)
                .build();
        hotelRoom2.setHotel(hotel);
        hotelRoomRepository.save(hotelRoom2);

        String newReservationJson1 = "{ \"roomId\": " + hotelRoom2.getId() + ", \"fromDate\": \""+toDateInFuture+"\", \"toDate\": \""+fromDateInFuture+"\"}";

        mockMvc.perform(post(API_BOOKING_ROOM + owner.getId())
                .content(newReservationJson1)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }
    @Test
    @DisplayName("Should create and return reservation")
    void shouldCreateAndReturnReservation() throws Exception {
        String newReservationJson = "{ \"roomId\": " + hotelRoom.getId() + ", \"fromDate\": \""+fromDateInFuture+"\", \"toDate\": \""+toDateInFuture+"\"}";

        mockMvc.perform(post(API_BOOKING_ROOM + owner.getId())
                .content(newReservationJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mockMvc.perform(get(API_BOOKING_ROOM + owner.getId())
                .content(newReservationJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].roomId", is(hotelRoom.getId().intValue())))
                .andExpect(jsonPath("$[0].fromDate", is(fromDateInFuture.toString())))
                .andExpect(jsonPath("$[0].toDate", is(toDateInFuture.toString())));
    }
    @Test
    @DisplayName("Should create, delete and return no reservation")
    void shouldCreateDeleteAndReturnNoReservation() throws Exception {
        String newReservationJson = "{ \"roomId\": " + hotelRoom.getId() + ", \"fromDate\": \""+fromDateInFuture+"\", \"toDate\": \""+toDateInFuture+"\"}";

        MvcResult mvcResult = mockMvc.perform(post(API_BOOKING_ROOM + owner.getId())
                .content(newReservationJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        mockMvc.perform(delete(API_BOOKING_ROOM + mvcResult.getResponse().getContentAsString()))
                .andExpect(status().isOk());

        mockMvc.perform(get(API_BOOKING_ROOM + owner.getId())
                .content(newReservationJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));
    }
}
