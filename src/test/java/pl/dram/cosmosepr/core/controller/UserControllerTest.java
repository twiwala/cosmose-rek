package pl.dram.cosmosepr.core.controller;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import pl.dram.cosmosepr.BaseIntegrationTest;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class UserControllerTest extends BaseIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("Should register customer with correct data")
    public void shouldRegisterCustomer() throws Exception {
        String newCustomerJson = "{\"email\": \"test@test.pl\",\"password\": \"testpass\"}";

        mockMvc.perform(post("/users/register")
                .content(newCustomerJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", greaterThanOrEqualTo(0)))
                .andExpect(jsonPath("$.email", is("test@test.pl")));
    }

    @Test
    @DisplayName("Should not register customer without password")
    public void shouldNotRegisterCustomerWithoutPassword() throws Exception {
        String newCustomerJson = "{\"email\": \"test@test.pl\",\"password\": \"\"}";

        mockMvc.perform(post("/users/register")
                .content(newCustomerJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    @DisplayName("Should not register customer without email")
    public void shouldNotRegisterCustomerWithoutEmail() throws Exception {
        String newCustomerJson = "{\"email\": \"\",\"password\": \"testpass\"}";

        mockMvc.perform(post("/users/register")
                .content(newCustomerJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }
}
